# Como Criar um ebuild no Gentoo

![Como Criar um ebuild no Gentoo](img/diagram.png)

Um arquivo [ebuild](https://wiki.gentoo.org/wiki/Ebuild) é um arquivo de texto, usado pelos gerenciadores de pacotes do Gentoo, que identifica um pacote de software específico e como o gerenciador de pacotes do Gentoo deve lidar com ele. Ele usa um estilo de sintaxe bash e é padronizado através da versão [EAPI](https://wiki.gentoo.org/wiki/EAPI).

## Introdução

O Gentoo Linux usa ebuilds como o formato de gerenciamento de pacotes para títulos de software individuais. Esses ebuilds contêm metadados sobre o software (o nome e a versão do software, que o software usa e a home page), informações de dependência (tanto em tempo de criação quanto em tempo de execução) e instruções sobre como lidar com o software (configure, construa, instale, teste ...).

## Desenvolvimento

Quando você configurou o Gentoo, você provavelmente se lembra que teve que baixar e descompactar um [snapshot](http://distfiles.gentoo.org/snapshots/) repositório do Gentoo. Este snapshot (que você atualiza quando você executa o `emerge --sync` ) está cheio de ebuilds, é o [repositório](https://wiki.gentoo.org/wiki/Ebuild_repository) do Gentoo e uma vez descompactado geralmente está localizado em [/usr/portage](https://wiki.gentoo.org/wiki//usr/portage).

> Se você decidir criar um ebuild dentro de `/usr/portage` , ex.: `/usr/portage/ola-mundo.ebuild` , ao rodar o `emerge --sync` ele será automaticamente deletado, existe regras para que o mesmo seja aceito na árvore de diretórios do [Portage](https://wiki.gentoo.org/wiki/Portage).

### Criando um ebuild básico

> É necessário possuir privilégios de **sudo** ou **root**

1. Criar o diretório que ficará o ebuild (geralmente ele possui o mesmo nome do ebuild):

```ebuild
mkdir -p /usr/local/portage/app-misc/ola-mundo
```

> Perceba que precisamos criar em **/usr/local/** e não em ~/usr/portage/~

Em seguida use o comando `cd $_` para entrar no diretório criado, a variável **$_** guarda o último argumento executado.

2. Dentro o diretório criado anteriormente vamos criar abrindo um arquivo com o [Vim](https://wiki.gentoo.org/wiki/Vim) se você tiver o plugin <https://github.com/gentoo/gentoo-syntax> instalado o arquivo terá a sintaxe do ebuild colorizada.

```ebuild
vim ./ola-mundo-1.0.ebuild
```

> Perceba que o esquema e o cabeçalho do ebuild já foi autocompletado . O mesmo (somente o cabeçalho) também poderia ser criado copiando do diretório do Portage com o seguinte comando: `cp /usr/portage/header.txt ./ola-mundo-1.0.ebuild` 

```ebuild
# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION=""
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
```

#### Vamos entender cada linha do Nosso arquivo

+ **# Copyright ... License v2** -  Esse é o cabeçalho obrigatório nos ebuilds, o qual falamos anteriormente, informa sobre os autores e a licença;
+ **EAPI=7** - Informa qual a regra de sintaxe para o ebuild que deve ser aceitar. É, na verdade, a versão do [Package Manager Specification (PMS)](https://wiki.gentoo.org/wiki/Package_Manager_Specification) à qual o arquivo obedece. As versões do EAPI (e seu significado) são definidas na Especificação do gerenciador de pacotes. Atualmente, os EAPIs de [0 a 7](https://devmanual.gentoo.org/ebuild-writing/eapi/) são definidos e oficialmente suportados; 
+ **DESCRIPTION** - Descreve resumidamente o conteúdo do ebuild, melhor dizendo: o que é o pacote que será instalado/compilado;
+ **HOMEPAGE** - Informa a página oficial do pacote;
+ **SRC_URI** - Informa O código fonte que baixaremos, hospedado pelo desenvolvedor que escreveu esta documentação, o caminho de uma arquivo compactado;
+ **LICENSE** - Informa qual a licença do pacote;
+ **SLOT** - Informa quantas versões podem ser instaladas do mesmo pacote. Essa variável não pode ficar vazia(~SLOT=""~) e também não pode deixar de ser especificada, se o não for usado, precisa ser declarado com  valor 0, ex.: `SLOT="0"`;
+ **KEYWORDS** - Informa quais plataformas o pacote pode ser instalado, ex.: arm, sparc,... no caso acima, é possível instalar em AMD 64-bit ou x86 (32-bit), usa-se com o símbolo *til*(~) na frente, outros exemplos: `~alpha ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc`;
+ **IUSE** - Informa as flags que pode/deve ser usada para compilar o pacote. Geralmente o usuário pode mencioná-las usando a variável *USE* , inibindo ou declando na linha de comando ou diretamente no arquivo `make.conf`;
+ **DEPEND** - Lista de [depências](https://devmanual.gentoo.org/general-concepts/dependencies/) essenciais para que o pacote seja construído (tempo de compilação);
+ **RDEPEND** - Lista de dependências para que o pacote seja executado (tempo de execução), perceba que a variável *DEPEND* já está inclusa;
+ **BDEPEND** - Uma lista das dependências de construção do pacote [CBUILD](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Full#Variables) (plataforma que você está compilando), usado somente na EAPI 7;

> Lembrando que esse é um exemplo básico, existem outras variáveis que podem ser consultadas no endereço: <https://devmanual.gentoo.org/ebuild-writing/variables/>. Para mais informações sobre esquema, veja o arquivo: `/usr/portage/skel.ebuild` .

O nosso ebuild ficou com sua versão final da seguinte maneira:

```ebuild
# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Imprime Olá, mundo em Shell Script"
HOMEPAGE="http://terminalroot.com.br/"
SRC_URI="http://terminalroot.com.br/downs/ola-mundo.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
```

> Perceba que as variáveis que não foram declaradas no nosso ebuild, não são essenciais para o funcionamento do mesmo.

Conteúdo do nosso `ola-mundo` (perceba que está sem a extensão *.sh*):

```bash
#!/usr/bin/env bash
# Exemplos simples para ebuild
# <terminalroot.com.br>

ola_mundo(){
	printf "%s\n" "Olá mundo!"	
}

ola_mundo
```

Crie um diretório de nome: `ola-mundo-1.0`, e adicione o o shell script dentro dele . Depois é só compactar e fazer o upload para um endereço da web: 
`tar zcvf ola-mundo-1.0.tar.gz ola-mundo-1.0/`

Agora rode o seguinte comando: `ebuild ola-mundo-1.0.ebuild manifest clean merge` . Perceba que primeiro tenta baixar nosso arquivo de um espelho, mas como não está nos espelhos do Gentoo, ele será baixado do valor *SRC_URI* especificado e salva no diretório `/usr/portage/distfiles/ola-mundo-1.0.tar.gz`. Então, quando ele tiver o arquivo, ele pode criar um manifesto (uma arquivo de nome `Manifest` dentro do diretório onde está o ebuild), isso contém um hash de nosso ebuild e esse arquivo baixado para garantir a integridade, de modo que a corrupção produza erros.

Então, o processo emerge entra em ação, a integridade é verificada primeiro. Então, podemos ver que o arquivo que baixamos é automaticamente descompactado , o que é muito útil, já que não precisamos mais implementá-lo. Podemos mudar esse comportamento substituindo sua função ( `src_unpack` ), definindo algumas variáveis  usando [eclasses](https://wiki.gentoo.org/wiki/Eclass) que definem tal comportamento; mas não precisamos fazer isso neste caso.

Mas a instalação não ocorre, pois precisamos dizer ao ebuild onde instalar nosso arquivo.

#### Versão final do ebuild

Para o ebuild instalar o arquivo final, precisamos atribuir uma [Função Ebuild](https://devmanual.gentoo.org/ebuild-writing/functions) que define onde deve ser instalado, para isso utilizaremos a função `src_install` em conjunto com outra função [dobin](https://devmanual.gentoo.org/function-reference/install-functions/) que instala um binário em `/usr/bin` , e define o modo de arquivo para *0755* e define a propriedade para superusuário ou seu equivalente no sistema ou instalação em mãos, ou seja, `src_install` é necessária para que o nosso shell script ola-mundo seja posteriormente colocado em `/usr/bin` e tornado executável:

```bash
src_install(){
    dobin ola-mundo
}
```

O conteúdo final do nosso ebuild, ficou:

```ebuild
# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Imprime Olá, mundo em Shell Script"
HOMEPAGE="http://terminalroot.com.br/"
SRC_URI="http://terminalroot.com.br/downs/ola-mundo.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

src_install(){
	dobin ola-mundo
}
```

Após adicionar ao ebuild a função acima, vamos rodar o comando novamente:

```bash
ebuild ola-mundo-1.0.ebuild manifest clean merge
```

Se tudo der certo, basta rodar no terminal: `ola-mundo` e verás a saída: **Olá mundo!** .

Se quiser desinstalar, use o emerge: `emerge -C ola-mundo` e se quiser também limpar o distfiles: `rm /usr/portage/ola-mundo*`

Não precisa remover o arquivo `Manifest` , a menos que você queira.

Saiba mais: <https://wiki.gentoo.org/wiki/Basic_guide_to_write_Gentoo_Ebuilds>
